(function ($) {
  Drupal.behaviors.exampleModule = {
    attach: function (context, settings) {
      // This code will run, on load, even in overlay!!!!!!
      $('#edit-poetic-toolkit-mobile-viewport-setting').change(function(){
        $('#edit-poetic-toolkit-enable-webapp-setting').attr('checked', false);
      });

      $('#edit-poetic-toolkit-mobile-viewport-setting').click(function() {
      	if ($("#edit-poetic-toolkit-enable-webapp-setting").is(":checked")) {
      		$("#poetic-toolkit-admin-settings .vertical-tabs").show();
      	} else {
      		$("#poetic-toolkit-admin-settings .vertical-tabs").hide();
      	}
      });

      $('#edit-poetic-toolkit-enable-webapp-setting').click(function() {
      	if ($("#edit-poetic-toolkit-enable-webapp-setting").is(":checked")) {
      		$("#poetic-toolkit-admin-settings .vertical-tabs").show();
      	} else {
      		$("#poetic-toolkit-admin-settings .vertical-tabs").hide();
      	}
      });
    }
  };
})(jQuery);
