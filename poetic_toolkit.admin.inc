<?php

function poetic_toolkit_admin_settings() {

	$viewport_setting_description = "Enable this to force the viewport to enable viewport 'width=device-width' and set the scale to 1.0. <strong>This MUST be enabled for other settings in this module to function.</strong>";

	$webapp_setting_description = "Enable this to allow for mobile devices to save the page as a bookmark and load into a webapp wrapper (for iOS and Android).";

	$form['poetic_toolkit_mobile_viewport_setting'] = array(
		'#type' => 'checkbox',
		'#title' => t('Enable Mobile Viewport'),
		'#description' => t($viewport_setting_description),
		'#default_value' => variable_get('poetic_toolkit_mobile_viewport_setting', TRUE),
	);

	$form['poetic_toolkit_enable_webapp_setting'] = array(
		'#type' => 'checkbox',
		'#title' => t('Enable Mobile Web App'),
		'#description' => t($webapp_setting_description),
		'#default_value' => variable_get('poetic_toolkit_enable_webapp_setting', FALSE),
		'#states' => array('visible' => array(':input[name="poetic_toolkit_mobile_viewport_setting"]' => array('checked' => TRUE),),),
	);


  $form['image_uploads'] = array(
    '#type' => 'vertical_tabs',
  );


//
// START iPADS
//
  $form['iPad Images'] = array(
    '#type' => 'fieldset',
    '#title' => t('iPad Image Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'image_uploads',
  );

  //
  //  iPad Logos
  //
  $form['iPad Images']['Logos'] = array(
    '#type' => 'fieldset',
    '#title' => t('iPad Logos'),
    '#description' => t('Please upload the largest image possible. Images will be automatically scaled down to the dimensions underneath the file upload field.'),
  );

  $form['iPad Images']['Logos']['poetic_toolkit_high_pad_logo_7'] = array(
    '#title' => t('Retina iPad (iOS 7)'),
    '#type' => 'managed_file',
    '#description' => t('152x152'),
    '#default_value' => variable_get('poetic_toolkit_high_pad_logo_7', ''),
    '#upload_location' => 'public://poetic_toolkit_uploads/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      'file_validate_image_resolution' => array('300x300'),
    ),
  );

  $form['iPad Images']['Logos']['poetic_toolkit_high_pad_logo_6'] = array(
    '#title' => t('Retina iPad (iOS 6)'),
    '#type' => 'managed_file',
    '#description' => t('144x144'),
    '#default_value' => variable_get('poetic_toolkit_high_pad_logo_6', ''),
    '#upload_location' => 'public://poetic_toolkit_uploads/',
    '#upload_validators' => array(
      'file_validate_extensions' => array('gif png jpg jpeg'),
      'file_validate_image_resolution' => array('300x300'),
    ),
  );
  
  poetic_toolkit_resize_images($form);

	return system_settings_form($form);
}

// function poetic_toolkit_resize_images($form) {
//   $containers_array = array('iPad Images', 'iPhone Images');
//   $image_types_array = array('Logos', 'Start Screens');
//   foreach($containers_array as $container_key => $container) {
//     foreach($image_types_array as $image_type_key => $image_type) {
//       foreach($form[$container][$image_type] as $key => $field) {
//         if (is_array($field)) {
//           $fid = variable_get($key, FALSE);
//           $file = file_load($fid);
//           $target_dim = explode("x", $field['#description']);
//           if ($file) {
//             $image = image_load($file->uri);
//             image_scale_and_crop($image, $target_dim[0], $target_dim[1]);
//             image_save($image);
//           }
//         }
//       }
//     }
//   }
}
